# Quaility Assurance of Go

## Contenido

 * [Introducción](https://gitlab.com/perla.maciel/quality-assurance-for-go#introducci%C3%B3n)
 * [Propuesta](https://gitlab.com/perla.maciel/quality-assurance-for-go#propuesta)
   * [GoDoc](https://gitlab.com/perla.maciel/quality-assurance-for-go#godoc)
   * [Testing](https://gitlab.com/perla.maciel/quality-assurance-for-go#testing)
   * [Gofmt](https://gitlab.com/perla.maciel/quality-assurance-for-go#gofmt)
   * [Gocyclo](https://gitlab.com/perla.maciel/quality-assurance-for-go#gocyclo)
   * [Golint](https://gitlab.com/perla.maciel/quality-assurance-for-go#golint)
 * [License](https://gitlab.com/perla.maciel/quality-assurance-for-go#licencia)

## Introducción

Este repositorio propone algunas herramientas para automatizar con la ayuda de un CI las mejores practicas en la codificación al utilizar el lenguaje de Go esto con la finalidad de tener un código de calidad, que sea mantenible y estandarizado.

## Propuesta

Se sugiere utilizar las siguientes herramientas las cuales algunas vienen incluidas en la paquetería estandar de Go y unas otras son proyectos open source de Go:

 * **GoDoc**: Es un paquete que esta incluido en la paquetería estandar de Go, aloja la documentación de los paquetes Go en Bitbucket, GitHub, Google Project Hosting y Launchpad. GoDoc muestra la documentación para GOOS = linux a menos que se indique lo contrario en la parte inferior de la página de documentación.
 * **Testing**: Proporciona soporte para las pruebas automatizadas de paquetes Go. Está diseñado para ser utilizado con el comando "go test", que automatiza la ejecución de cualquier función.
 * **Gofmt**: Formatea los programas de Go. Utiliza tabuladores para sangría y espacios en blanco para alineación. La alineación supone que un editor está utilizando una fuente de ancho fijo. Es un paquete estandar de Go.
 * **Gocyclo**: Calcula las complejidades ciclomáticas de las funciones en el código fuente de Go. Es un paquete open source del usuario @fzipp.
 * **Golint**: Es un paquete oficial de Go el cual es una guía para el código fuente de Go.

### GoDoc

#### Instalación

Ya esta incluido en la instalación de Go por detecto.

#### Uso

##### * Agregar un paquete a GoDoc

GoDoc genera documentación desde el código fuente de Go. Las pautas para escribir documentación para la herramienta godoc se aplican a GoDoc.

Es importante escribir un buen resumen del paquete en la primera oración del comentario del paquete. GoDoc indexa la primera oración y muestra la primera oración en las listas de paquetes.

Para agregar un paquete a GoDoc, busque el paquete por ruta de importación. Si GoDoc aún no tiene la documentación para el paquete, GoDoc buscará la fuente del sistema de control de versiones sobre la marcha y agregará la documentación.

GoDoc comprueba si hay actualizaciones de paquetes una vez al día. Puede obligar a GoDoc a actualizar la documentación inmediatamente haciendo clic en el enlace de actualización en la parte inferior de la página de documentación del paquete.

GoDoc rastrea las importaciones de paquetes y los directorios secundarios para encontrar nuevos paquetes.

##### * Remover un paquete de GoDoc

GoDoc elimina automáticamente los paquetes eliminados del sistema de control de versiones cuando GoDoc comprueba si hay actualizaciones en el paquete. Puede forzar que GoDoc elimine un paquete eliminado inmediatamente haciendo clic en el enlace de actualización en la parte inferior de la página de documentación del paquete. Si no desea que GoDoc muestre la documentación de su paquete, envíe el correo a golang-dev@googlegroups.com con la ruta de importación de la ruta del paquete que desea eliminar.

#### Más acerca de GoDoc

Ver documentación oficial [Godoc](https://godoc.org/)

### Testing

#### Uso

Automatiza la execución de las pruebas de cualquier función de la forma:

```golang
func TestXxx(*testing.T)
```

donde Xxx no comienza con una letra minúscula. El nombre de la función sirve para identificar la rutina de prueba.

Dentro de estas funciones, use el Error, Fail o métodos relacionados para señalar el fallo.

Para escribir un nuevo conjunto de pruebas, cree un archivo cuyo nombre finalice _test.go que contenga las funciones de TestXxx como se describe aquí. Coloque el archivo en el mismo paquete que el que se está probando. El archivo se excluirá de las compilaciones de paquetes normales, pero se incluirá cuando se ejecute el comando "go test". Para obtener más detalles, ejecute "go help test" y "go help testflag".

Las pruebas y los puntos de referencia pueden omitirse si no son aplicables con una llamada al método Skip de * T y * B:

```golang
func TestTimeConsuming(t *testing.T) {
    if testing.Short() {
        t.Skip("skipping test in short mode.")
    }
    ...
}
```

##### * Benchmarks

Funciones de la forma:

```golang
func BenchmarkXxx(*testing.B)
```

se consideran puntos de referencia y se ejecutan con el comando "go test" cuando se proporciona la bandera -bench. Los puntos de referencia se ejecutan de forma secuencial.

Para obtener una descripción de las banderas de testing, consulte https://golang.org/cmd/go/#hdr-Testing_flags

Un ejemplo de función de referencia se ve así:

```golang
func BenchmarkHello(b *testing.B) {
    for i := 0; i < b.N; i++ {
        fmt.Sprintf("hello")
    }
}
```

La función de referencia debe ejecutar el código de destino b.N veces. Durante la ejecución del índice de referencia, b.N se ajusta hasta que la función del índice de referencia dura el tiempo suficiente para que se pueda cronometrar de manera confiable. La salida

```golang
BenchmarkHello    10000000    282 ns/op
```

significa que el bucle se ejecutó 10000000 veces a una velocidad de 282 ns por bucle.

##### * Examples

El paquete también se ejecuta y verifica el código de ejemplo. Las funciones Example pueden incluir un comentario de línea final que comienza con "Output:" y se compara con la salida estándar de la función cuando se ejecutan las pruebas. (La comparación ignora los espacios iniciales y finales.) Estos son ejemplos de un example:

```golang
func ExampleHello() {
    fmt.Println("hello")
    // Output: hello
}

func ExampleSalutations() {
    fmt.Println("hello, and")
    fmt.Println("goodbye")
    // Output:
    // hello, and
    // goodbye
}
```
#### Más acerca de Testing

Ver documentación oficial [Testing](httpshttps://golang.org/pkg/testing/)

### Gofmt

#### Uso

```golang
gofmt [flags] [path ...]
```
Las banderas que se puden utilizar estan en la página de la documentación oficial: [Gofmt](https://golang.org/cmd/gofmt/)

#### Ejemplos

Para revisar parentesis inecesarios

```golang
gofmt -r '(a) -> a' -l *.go
```
Para remover los parentesis

```golang
gofmt -r '(a) -> a' -w *.go
```

### Gocyclo

#### Instalación

Para instalar corre el siguiente comando en consola:

```golang
$ go get github.com/fzipp/gocyclo
```

y coloca el binario resultante en uno de sus directorios PATH si $GOPATH/bin no está ya en su PATH.

#### Uso

##### Flags



      -over N   show functions with complexity > N only and
                return exit code 1 if the output is non-empty
      -top N    show the top N most complex functions only
      -avg      show the average complexity


Ejecuta el siguiente comando en consola:

```golang
$ gocyclo [<flag> ...] <Go file or directory> ...
```

#### Ejemplos

```golang
$ gocyclo .
$ gocyclo main.go
$ gocyclo -top 10 src/
$ gocyclo -over 25 docker
$ gocyclo -avg .
```

Los campos de salida para cada linea son

```golang
<complexity> <package> <function> <file:row:column>
```

#### Más acerca de Gocyclo

Ver documentación oficial [Gocyclo](httpshttpshttps://github.com/fzipp/gocyclo)

### Golint

#### Instalación

Golint requiere una versión de Go soportada [Supported](https://golang.org/doc/devel/release.html#policy)

```golang
$ go get -u golang.org/x/lint/golint
```

#### Uso

Invoque golint con uno o más nombres de archivos, directorios o paquetes nombrados por su ruta de importación. Golint usa la misma sintaxis de ruta de importación que el comando go y, por lo tanto, también admite rutas de importación relativas como ./.... Además, el comodín ... se puede usar como sufijo en rutas de archivo relativas y absolutas para recuperarlas.

#### Proposito

Golint se diferencia de gofmt. Gofmt reformatea el código fuente de Go, mientras que Golint imprime los errores de estilo.

Golint se diferencia del govet. Govet se preocupa por la corrección, mientras que golint se ocupa del estilo de codificación. Golint se usa en Google y busca coincidir con el estilo aceptado del proyecto Go de código abierto.

Las sugerencias hechas por golint son exactamente eso: sugerencias. Golint no es perfecto, y tiene falsos positivos y falsos negativos. No trate su producción como un estándar de oro. En resumen, esta herramienta no es, y nunca será, lo suficientemente confiable para que sus sugerencias se apliquen automáticamente.

#### Más acerca de Golint

Ver documentación oficial [Golint](httpshttpshttpshttps://github.com/golang/lint)


### Quick-Check

#### Instalación

Package [quick](https://golang.org/pkg/testing/quick/) pertenece a la paqueteria de testing

```golang
$ go get -u golang.org/pkg/testing/quick/
```

#### Funciones

##### func Check

```golang
func Check(f interface{}, config *Config) error
```

busca una entrada en "f", cualquier función que devuelva bool, tal que "f" devuelva false. Llama "f" repetidamente, con valores arbitrarios para cada argumento. Si "f" devuelve false en una entrada determinada, Check devuelve esa entrada como * CheckError. Por ejemplo:

```golang
func TestOddMultipleOfThree(t *testing.T) {
	f := func(x int) bool {
		y := OddMultipleOfThree(x)
		return y%2 == 1 && y%3 == 0
	}
	if err := quick.Check(f, nil); err != nil {
		t.Error(err)
	}
}
```

##### func CheckEqual

```golang
func CheckEqual(f, g interface{}, config *Config) error
```

##### func Value

```golang
func Value(t reflect.Type, rand *rand.Rand) (value reflect.Value, ok bool)
```

#### Más acerca de Quick-Check

Ver documentación oficial [Quick-Check](https://golang.org/pkg/testing/quick/#Check).

## Licencia

Ver licencia [MIT](https://gitlab.com/perla.maciel/quality-assurance-for-go/blob/master/LICENSE)
