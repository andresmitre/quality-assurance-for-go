package src

/*Funcion que retorna el promedio de un array de
calificaciones*/
func Average(xs []float64) float64 {
	total := 0.0
	for _, v := range xs {
		total += v
	}
	return total / float64(len(xs))
}

//Funcion que permite recibir varios enteros
func Add(args ...int) int {
	total := 0
	for _, v := range args {
		total += v
	}
	return total
}

func makeEvenGenerator() func() int {
	i := int(0)
	return func() (ret int) {
		ret = i
		i += 2
		return
	}
}

func Greater(n ...int) int {
	max := 0
	for i:=0 ; i < len(n) ; i++ {
		if n[i] > max {
			max = n[i]
		}
	} 
	return max
} 

func Absolute(n float64) float64 {
    if n < 0 {
        return n * (-1)
    }
    return n
}

func Ex2(n float64) float64 {
    return n * n 
}
